# Snake-R

This project is a simple Snake clone that can be played in the browser. My goal was to learn how to develop and use WebAssembly with Rust. This is what it looks like:

<img src="./preview.gif" alt="Gif of the game in the browser" />