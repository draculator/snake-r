const { join } = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
	entry: './www/index.ts',
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				use: 'ts-loader',
				exclude: /node_modules/,
			},
			{
				test: /\.wasm$/,
				type: "asset/inline",
			},
		],
	},
	resolve: {
		extensions: ['.tsx', '.ts', '.js'],
	},
	output: {
		path: join(__dirname, 'dist'),
		filename: 'app.js',
		clean: true,
	},
	mode: 'development',
	devServer: {
		port: 3000,
	},
	plugins: [
		new CopyWebpackPlugin({
			patterns: [
				{ from: './www/index.html', to: './' },
			]
		}),
	]
}