use rand::{ thread_rng, Rng };
use wasm_bindgen::prelude::*;

#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
#[derive(PartialEq)]
pub enum Direction {
    Up,
    Right,
    Down,
    Left,
}

#[wasm_bindgen]
#[derive(Clone, Copy)]
pub enum GameState {
    Running,
    Lost,
    Won,
}

#[derive(PartialEq, Clone, Copy)]
pub struct SnakeCell(usize);

struct Snake {
    body: Vec<SnakeCell>,
    direction: Direction,
}

impl Snake {
    fn new(spawn_index: usize, size: usize) -> Snake {
        let mut body = vec!();

        for i in 0..size {
            body.push(SnakeCell(spawn_index - i));
        }

        Snake {
            body,
            direction: Direction::Right,
        }
    }
}

#[wasm_bindgen]
pub fn generate_random_unsigned_number(size: usize) -> usize {
    let mut rng = thread_rng();
    rng.gen_range(0..size)
}

#[wasm_bindgen]
pub struct World {
    size: usize,
    width: usize,
    snake: Snake,
    points: usize,
    state: Option<GameState>,
    food_cell: Option<usize>,
    next_cell: Option<SnakeCell>,
}

#[wasm_bindgen]
impl World {
    pub fn new(width: usize, snake_index: usize) -> World {
        let size = width * width;
        let snake = Snake::new(snake_index, 3);
        let food_cell = Self::generate_next_food_cell(size, &snake.body);

        World {
            size,
            width,
            snake,
            points: 0,
            state: None,
            next_cell: None,
            food_cell: Some(food_cell),
        }
    }

    pub fn start_game(&mut self) {
        self.state = Some(GameState::Running);
    }

    pub fn width(&self) -> usize {
        self.width
    }

    pub fn food_cell(&self) -> Option<usize> {
        self.food_cell
    }

    pub fn points(&self) -> usize {
        self.points
    }

    pub fn snake_head_index(&self) -> usize {
        self.snake.body[0].0
    }

    pub fn snake_length(&self) -> usize {
        self.snake.body.len()
    }

    pub fn snake_cells(&self) -> *const SnakeCell {
        self.snake.body.as_ptr()
    }

    pub fn game_state(&self) -> Option<GameState> {
        self.state
    }

    fn generate_next_food_cell(size: usize, snake_body: &Vec<SnakeCell>) -> usize {
        loop {
            let food_cell = generate_random_unsigned_number(size);
            if !snake_body.contains(&SnakeCell(food_cell)) {
                return food_cell;
            }
        }
    }

    pub fn change_snake_direction(&mut self, direction: Direction) {
        let next_cell = self.generate_next_snake_cell(&direction);

        // Prevent the snake from reversing direction
        if self.snake.body[1].0 == next_cell.0 {
            return;
        }

        self.next_cell = Some(next_cell);
        self.snake.direction = direction;
    }

    fn generate_next_snake_cell(&self, direction: &Direction) -> SnakeCell {
        let snake_index = self.snake_head_index();
        let row = snake_index / self.width;

        return match direction {
            Direction::Up => {
                let top_edge = snake_index - row * self.width;
                if snake_index == top_edge {
                    SnakeCell(self.size - self.width + top_edge)
                } else {
                    SnakeCell(snake_index - self.width)
                }
            }
            Direction::Right => {
                let right_edge = (row + 1) * self.width;
                let next_cell = snake_index + 1;
                if next_cell == right_edge {
                    SnakeCell(right_edge - self.width)
                } else {
                    SnakeCell(next_cell)
                }
            }
            Direction::Down => {
                let bottom_edge = snake_index + (self.width - row) * self.width;
                if snake_index + self.width == bottom_edge {
                    SnakeCell(bottom_edge - (row + 1) * self.width)
                } else {
                    SnakeCell(snake_index + self.width)
                }
            }
            Direction::Left => {
                let left_edge = row * self.width;
                if snake_index == left_edge {
                    SnakeCell(left_edge + self.width - 1)
                } else {
                    SnakeCell(snake_index - 1)
                }
            }
        };
    }

    pub fn step(&mut self) {
        match self.state {
            Some(GameState::Running) => {
                let temp = self.snake.body.clone();

                match self.next_cell {
                    Some(cell) => {
                        self.snake.body[0] = cell;
                        self.next_cell = None;
                    }
                    None => {
                        self.snake.body[0] = self.generate_next_snake_cell(&self.snake.direction);
                    }
                }

                let len = self.snake.body.len();

                for i in 1..len {
                    self.snake.body[i] = SnakeCell(temp[i - 1].0);
                }

                if self.snake.body[1..len].contains(&self.snake.body[0]) {
                    self.state = Some(GameState::Lost);
                }

                if self.food_cell == Some(self.snake_head_index()) {
                    if self.snake_length() < self.size {
                        self.points += 1;
                        self.food_cell = Some(
                            Self::generate_next_food_cell(self.size, &self.snake.body)
                        );
                    } else {
                        self.state = Some(GameState::Won);
                    }
                    self.snake.body.push(SnakeCell(self.snake.body[1].0));
                }
            }
            _ => {}
        }
    }
}