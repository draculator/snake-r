import init, {
  World,
  Direction,
  GameState,
  generate_random_unsigned_number as randomIndex,
} from 'snake_r';
(async () => {
  const FRAMES_PER_SECOND = 6;
  const WORLD_WIDTH = 32;
  const CELL_WIDTH = 10;
  const SNAKE_HEAD_COLOR = '#00FFAA';
  const SNAKE_COLOR = '#00FF00';
  const FOOD_COLOR = '#FF3902';

  // Holds all functions related to painting onto the canvas
  const Painter = {
    drawGame() {
      this.drawGrid();
      this.drawFood();
      this.drawSnake();
    },

    drawGrid() {
      ctx.beginPath();
      for (let x = 0; x < worldWidth + 1; x++) {
        ctx.moveTo(CELL_WIDTH * x, 0);
        ctx.lineTo(CELL_WIDTH * x, worldWidth * CELL_WIDTH);
      }
      for (let y = 0; y < worldWidth + 1; y++) {
        ctx.moveTo(0, CELL_WIDTH * y);
        ctx.lineTo(worldWidth * CELL_WIDTH, CELL_WIDTH * y);
      }
      ctx.stroke();
    },

    drawSnake() {
      const snakeCells = new Uint32Array(
        memory.buffer,
        world.snake_cells(),
        world.snake_length()
      );

      snakeCells
        .filter((cell, i) => !(i > 0 && cell === snakeCells[0]))
        .forEach((cell, i) => {
          this.drawCell(cell, i === 0 ? SNAKE_HEAD_COLOR : SNAKE_COLOR);
        });
    },

    drawFood() {
      const cell = world.food_cell();
      this.drawCell(cell, FOOD_COLOR);
    },

    drawCell(cell: number, color: string) {
      const col = cell % WORLD_WIDTH;
      const row = Math.floor(cell / WORLD_WIDTH);

      ctx.beginPath();
      ctx.fillStyle = color;
      ctx.fillRect(col * CELL_WIDTH, row * CELL_WIDTH, CELL_WIDTH, CELL_WIDTH);
      ctx.stroke();
    },
  };

  const { memory } = await init();

  const snakeSpawnIndex = randomIndex(WORLD_WIDTH * WORLD_WIDTH);
  const world = World.new(WORLD_WIDTH, snakeSpawnIndex);
  const worldWidth = CELL_WIDTH * WORLD_WIDTH;

  const canvas = <HTMLCanvasElement>document.querySelector('#canvas');
  const playButton = <HTMLCanvasElement>document.querySelector('#btn-play');
  const pointsPill = <HTMLCanvasElement>document.querySelector('#points-pill');

  const ctx = canvas.getContext('2d');

  canvas.height = worldWidth;
  canvas.width = worldWidth;

  playButton.addEventListener('click', () => {
    startGame();
  });

  document.addEventListener('keydown', (e: KeyboardEvent) => {
    const directions = new Map<string, Direction>([
      ['KeyW', Direction.Up],
      ['ArrowUp', Direction.Up],
      ['KeyD', Direction.Right],
      ['ArrowRight', Direction.Right],
      ['KeyS', Direction.Down],
      ['ArrowDown', Direction.Down],
      ['KeyA', Direction.Left],
      ['ArrowLeft', Direction.Left],
    ]);
    world.change_snake_direction(directions.get(e.code));
  });

  function startGame() {
    if (world.game_state() === undefined) {
      playButton.textContent = 'Reset';
      world.start_game();
      run();
    } else {
      location.reload();
    }
  }

  function checkState() {
    switch (world.game_state()) {
      case GameState.Running:
        break;
      case GameState.Lost:
        alert('You Loose');
        break;
      case GameState.Won:
        alert('You Win!');
        break;
    }
  }

  const run = async () => {
    if (world.game_state() !== GameState.Running) {
      return;
    }
    await new Promise((resolve) => {
      setTimeout(resolve, 1000 / FRAMES_PER_SECOND);
    });
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    world.step();
    Painter.drawGame();
    checkState();
    pointsPill.textContent = world.points().toString();
    requestAnimationFrame(run);
  };

  Painter.drawGrid();
})();
